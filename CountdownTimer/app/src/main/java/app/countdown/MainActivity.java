package app.countdown;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {

    private TextView tv1;
    private Button startbtn, cancelbtn;
    private ToggleButton togbtn;
    private EditText myinputHours, myinputMinutes, myInputSecond;
    MediaPlayer mPlayer;

    int hours, minutes, seconds;

    CountDownTimer countDownTimer;

    private boolean isPaused = false;
    private boolean isCanceled = false;


    @Override
    public boolean onCreateOptionsMenu(Menu menu)   {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_settings:
                Intent intent = new Intent(this, ActivityTwo.class);
                startActivity(intent);
                break;
            case R.id.action_exit:
                finish();
                break;
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPlayer=MediaPlayer.create(this, R.raw.over_the_horizon);
        mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                stopPlay();
            }

            private void stopPlay() {
                mPlayer.stop();
                try {
                    mPlayer.prepare();
                    mPlayer.seekTo(0);
                } catch (Throwable t) {
                    mPlayer.seekTo(0);
                }
            }
        });

        tv1=(TextView)findViewById(R.id.tv1);
        startbtn=(Button)findViewById(R.id.startbtn);
        cancelbtn=(Button)findViewById(R.id.cancelbtn);
        togbtn=(ToggleButton)findViewById(R.id.toggleButton);
        cancelbtn.setEnabled(false);
        togbtn.setEnabled(false);
        myinputHours=(EditText)findViewById(R.id.inputHours);
        myinputMinutes=(EditText)findViewById(R.id.inputMinutes);
        myInputSecond=(EditText)findViewById(R.id.inputSecond);

        startbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                isPaused = false;
                isCanceled = false;

                if (myinputHours.getEditableText().toString().length() == 0
                        & myinputMinutes.getEditableText().toString().length() == 0
                        & myInputSecond.getEditableText().toString().length() == 0) {
                    Toast.makeText((getApplicationContext()), "Заполните хоть одно поле!",
                            Toast.LENGTH_LONG).show();
                } else{
                    startService(new Intent(MainActivity.this, TreyService.class));

                    startbtn.setEnabled(false);
                    cancelbtn.setEnabled(true);
                    togbtn.setEnabled(true);

                    if (myinputHours.getText().toString().equals("")) {
                        hours = 0;

                    } else {
                        hours = Integer.parseInt(myinputHours.getText().toString());
                    }

                    if (myinputMinutes.getText().toString().equals("")) {
                        minutes = 0;

                    } else {
                        minutes = Integer.parseInt(myinputMinutes.getText().toString());
                    }

                    if (myInputSecond.getText().toString().equals("")) {
                        seconds = 0;

                    } else {
                        seconds = Integer.parseInt(myInputSecond.getText().toString());
                    }

                    long countDownInterval = 1000;

                    int total = ((hours * 3600000) + (minutes * 60000) + (seconds * 1000));

                    countDownTimer = new CountDownTimer(total, countDownInterval) {

                        @Override
                        public void onTick(long millisUntilFinished) {

                            if (isPaused || isCanceled) {
                                onPause();
                                stopService(new Intent(MainActivity.this, TreyService.class));
                            } else {
                                tv1.setText(String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millisUntilFinished),
                                        TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)
                                                - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)
                                                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                                startService(new Intent(MainActivity.this, TreyService.class));

                            }

                        }

                        @Override
                        public void onFinish() {
                            tv1.setText("KABOOM");
                            stopService(new Intent(MainActivity.this, TreyService.class));
                            mPlayer.start();

                        }
                    }.start();


                }
            }
        });
//Глобально менял Мишка, с 2 кнопок (паузы и восстановления), на 1 кнопку переключатель
        togbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (togbtn.isChecked()) {
                    isPaused = true;
                } else {
                    isPaused = false;
                }
            }
        });
// Делал Антон, дополнял Мишка, после добавления кнопки переключателя
        cancelbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stopService(new Intent(MainActivity.this,TreyService.class));

                isCanceled = true;
                countDownTimer.cancel(); // подкоректировал Антон, т.к. не было завершения потока
                mPlayer.stop();
                tv1.setText("Таймер");
                startbtn.setEnabled(true);
                togbtn.setEnabled(false);
                cancelbtn.setEnabled(false);
            }
        });


    }


}
